FROM php:7.2-apache
COPY config/php.ini /usr/local/etc/php/
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libxslt-dev \
        libicu-dev \
        libxml2-dev \
        libpq-dev sqlite3 libsqlite3-dev libzip-dev\
        git unzip \
    && docker-php-ext-install -j$(nproc) iconv  \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install xsl \
    && docker-php-ext-install intl \
    && docker-php-ext-install pdo_pgsql pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-install soap \
    && docker-php-ext-install bcmath \
    && a2enmod rewrite \
    && service apache2 restart
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php && php composer-setup.php --install-dir=/usr/local/bin --filename=composer && rm composer-setup.php	
